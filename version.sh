#!/bin/sh
COMMIT_COUINT=$(git rev-list --count HEAD)
PACKAGE_VERSION=$(cat package.json |  jq '.version' | tr -d "\"\`'")
PACKAGE_VERSION="${PACKAGE_VERSION//.0/.$COMMIT_COUINT}"
echo $PACKAGE_VERSION
