FROM node:14-alpine

ARG SSH_KEY
RUN apk add git openssh-client

WORKDIR /app/

COPY . .

RUN mkdir -p -m 0600 ~/.ssh && ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
RUN ssh-agent sh -c 'echo $SSH_KEY | base64 -d | ssh-add - ; npm install'

RUN npm run build

CMD ["npm", "start"]
