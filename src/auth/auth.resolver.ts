import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { GraphQLString } from 'graphql';
import { User } from '../entity/entities-index';
import { UserService } from '../user/user.service';
import { AuthService } from './auth.service';
import { IpAddress } from './logged-user.decorator';
import { CreateUserRequest } from './request/create-user.request';
import { LoginUserRequest } from './request/login-user.request';
import { LoginedUserResponse } from './response/logined-user.response';

@Resolver(() => LoginedUserResponse)
export class AuthResolver {
  constructor(
    private readonly authService: AuthService,
  ) {}

  @Mutation(() => User)
  public async register(
    @Args('createUser', { type: () => CreateUserRequest })
    createUser: CreateUserRequest,
    @IpAddress() ipAddress: string,
  ) {
    return this.authService.register(createUser, ipAddress);
  }

  @Mutation(() => LoginedUserResponse)
  public async login(
    @Args('loginUser', { type: () => LoginUserRequest })
    request: LoginUserRequest,
    @IpAddress() ipAddress: string,
  ) {
    return this.authService.login(request, ipAddress);
  }

  @Mutation(() => LoginedUserResponse, {
    description: 'Get access token by refresh token',
  })
  public async refresh(
    @Args('refreshToken', { type: () => GraphQLString })
    refreshToken: string,
    @IpAddress() ipAddress: string,
  ) {
    return this.authService.refresh(refreshToken, ipAddress);
  }
}
