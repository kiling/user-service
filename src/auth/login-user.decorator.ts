import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { LoggedUser } from '../common/auth/model/logged-user';

export const GetLoggedUser = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
	const ctxq = GqlExecutionContext.create(ctx);
	return ctxq.getContext().req.user as LoggedUser;
});