import { Field, ObjectType } from "@nestjs/graphql";
import { UserRole } from "../../user/model/user-role";

@ObjectType()
export class LoginedUserResponse {
	@Field()
	email: string;

	@Field(() => UserRole)
	role: UserRole;

	@Field()
	expiresIn: string;

	@Field()
	accessToken: string;

	@Field()
	expiresInRefreshTocken: string;

	@Field()
	refreshToken: string;
}