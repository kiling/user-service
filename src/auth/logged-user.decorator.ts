import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';

export const IpAddress = createParamDecorator((data: unknown, ctx: ExecutionContext) => {
    const ctxg = GqlExecutionContext.create(ctx);
    const req = ctxg.getContext().req;
	let ip = "";
	if (req.ip)
		ip = req.ip
	if (ip) {
		let res = ip.split(":");
		if (res.length > 0)
			return res[res.length - 1];
	}
	return null;
});