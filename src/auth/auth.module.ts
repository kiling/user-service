import { Module } from '@nestjs/common';
import { UserLoginHistoryModule } from '../user-login-history/user-login-history.module';
import { TokenModule } from '../token/token.module';
import { UserModule } from '../user/user.module';
import { AuthResolver } from './auth.resolver';
import { AuthService } from './auth.service';

@Module({
	imports: [
		UserModule,
		TokenModule,
		UserLoginHistoryModule
	],
	controllers: [],
	providers: [AuthService, AuthResolver],
})
export class AuthModule {}