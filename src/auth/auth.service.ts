import { Injectable, Logger } from '@nestjs/common';
import { TokenService } from '../token/token.service';
import { User } from '../entity/entities-index';
import { UserService } from '../user/user.service';
import { CreateUserRequest } from './request/create-user.request';
import { LoginUserRequest } from './request/login-user.request';
import { LoginedUserResponse } from './response/logined-user.response';
import { JWT_SETTINGS } from '../common/auth';

@Injectable()
export class AuthService {
  private readonly log = new Logger(AuthService.name);

  constructor(
    private readonly userService: UserService,
    private readonly tokens: TokenService,
  ) {}
  private async createToken(
    user: User,
    ip: string,
  ): Promise<LoginedUserResponse> {
    this.log.debug(
      `Creating token for user ${user.id} with email ${user.email}`,
    );
    const accessToken = await this.tokens.generateAccessToken(user);
    const refreshToken = await this.tokens.generateRefreshToken(user, ip);
    return {
      email: user.email,
      role: user.role,
      expiresIn: accessToken.expiresIn,
      accessToken: accessToken.token,
      expiresInRefreshTocken: refreshToken.expiresIn,
      refreshToken: refreshToken.token,
    };
  }

  public async register(
    createUser: CreateUserRequest,
    ipAddres: string,
  ): Promise<User> {
    this.log.debug(`Creating user with username ${createUser.email}`);
    return await this.userService.create(createUser, ipAddres);
  }

  public async login(
    login: LoginUserRequest,
    ipAddres: string,
  ): Promise<LoginedUserResponse> {
    const user = await this.userService.tryToLogin(login.email, login.password, ipAddres);
    return this.createToken(user, ipAddres);
  }

  public async refresh(
    refreshToken: string,
    ipAddres: string,
  ): Promise<LoginedUserResponse> {
    let res = await this.tokens.createAccessTokenFromRefreshToken(refreshToken, ipAddres);

    this.tokens.generateAccessToken;
    return {
      expiresIn: res.expiresIn,
      expiresInRefreshTocken: JWT_SETTINGS.refresh.expiresIn,
      accessToken: res.token,
      refreshToken: refreshToken,
      role: res.user.role,
      email: res.user.email,
    };
  }
}
