import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';
import { Transform } from 'class-transformer';
import { ArgsType, Field, InputType } from '@nestjs/graphql';
import { UserErrorResponse } from '../../user/dto/response/user-error.response';
import { capitalize } from '../../common/helper/string-helper';

export enum UserError {
	MinNameLength = 'The number of characters must not be less than 1 character',
	MaxNameLength = 'The number of characters must not exceed 128 characters',
}

@InputType()
export class CreateUserRequest {
    @Field()
	@IsEmail({}, { message: UserErrorResponse.EMAIL_NOT_VALID_ERROR })
    @Transform(transform => transform.value.toLowerCase().trim())
	email: string;

    @Field()
	@IsString()
	@Transform(transform => capitalize(transform.value))
	@MinLength(1, {
		message: UserError.MinNameLength,
	})
	@MaxLength(128, {
		message: UserError.MaxNameLength,
	})
	firstName: string;

    @Field()
	@IsString()
	@Transform(transform => capitalize(transform.value))
	@MinLength(1, {
		message: UserError.MinNameLength,
	})
	@MaxLength(128, {
		message: UserErrorResponse.EMAIL_NOT_VALID_ERROR,
	})
	surName: string;

    @Field()
	@IsString()
	@Transform(transform => transform.value.trim())
	@MinLength(6, {
		message: UserErrorResponse.PASSWORD_NOT_VALID_ERROR,
	})
	@MaxLength(64, {
		message: UserErrorResponse.PASSWORD_NOT_VALID_ERROR,
	})
	password: string;
}
