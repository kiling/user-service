
import { Field, InputType } from '@nestjs/graphql';
import { Transform } from 'class-transformer';
import { IsEmail } from 'class-validator';
import { transform } from 'typescript';
import { UserErrorResponse } from '../../user/dto/response/user-error.response';

@InputType()
export class LoginUserRequest {
	@Field()
	@IsEmail({}, { message: UserErrorResponse.EMAIL_NOT_VALID_ERROR })
	@Transform(transform => transform.value.toLowerCase().trim())
	email: string;

	@Field()
	@Transform(transform => transform.value.trim())
	password: string;
}