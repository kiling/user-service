import * as entities from '../entity/entities-index';

const config = {
	type: 'postgres',
	schema: 'public',
	host: process.env.DB_HOST || 'localhost',
	port: process.env.DB_PORT || 5432,
	username: process.env.DB_USER || 'user',
	password: process.env.DB_PASSWORD || '',
	database: process.env.DB_NAME || 'postgres',
	synchronize: false,
	dropSchema: false,
	logging: 'true',
	entities: Object.values(entities),
};

export = config;