import 'dotenv/config';
import { INestApplication, Logger, ValidationError, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { registerEnumType } from '@nestjs/graphql';
import { AppModule } from './app.module';
import { OrderByOptions } from './common/pagination/pagination.config';
import { UserRole } from './user/model/user-role';
import { UserStatus } from './user/model/user-status';
import { ValidationException, ValidationFilter } from './common/validation';
import cors from 'cors';

export const IS_PROD = false;
export const ALLOWED_ORIGINS = (
  process.env.ALLOWED_ORIGINS ||
  'https://anyvoice.ru,https://dev.anyvoice.ru,https://api.anyvoice.ru,https://api.dev.anyvoice.ru'
).split(',');

function isAllowedOrigin(
  origin: string | undefined,
  callback: (err: Error | null, allow?: boolean) => void,
): void {
  if (!IS_PROD || !origin) {
    return callback(null, true);
  } else if (!ALLOWED_ORIGINS.includes(origin)) {
    const message = `The CORS policy for this site does not
                        allow access from the specified Origin ("${origin}").`;
    Logger.warn(message);
    return callback(new Error(message), false);
  } else return callback(null, true);
}

const initFilter = (app: INestApplication) => {
  app.useGlobalFilters(
    // new AllExceptionsFilter(),
    // new HttpExceptionsFilter(),
    new ValidationFilter()
  );
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      skipMissingProperties: true,
      exceptionFactory: (errors: ValidationError[]) => {
        throw new ValidationException(errors);
      },
    })
  );
};

async function bootstrap() {
  try {
    const app = await NestFactory.create(AppModule);
    app.use(cors({ origin: isAllowedOrigin }));

    registerEnumType(OrderByOptions, {
      name: 'OrderByOptions',
    });
    registerEnumType(UserStatus, {
      name: 'UserStatus',
    });
    registerEnumType(UserRole, {
      name: 'UserRole',
    });
    initFilter(app);
    await app.startAllMicroservicesAsync();
    var port = process.env.APP_PORT || 3000;
    await app.listen(port, () => {
      Logger.log('Listening at http://localhost:' + port);
    });
  } catch ({ message }) {
    Logger.error(`TypeORM connection error: ${message}`);
  }
}
bootstrap();
