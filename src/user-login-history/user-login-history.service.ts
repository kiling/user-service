import { Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { UserLoginHistory } from './model/user-login-history.entity';
import { UserTryLoginStatus } from './model/user-try-login-status';

@Injectable()
export class UserLoginHistoryService {
	private readonly log = new Logger(UserLoginHistoryService.name);

	constructor(
		@InjectRepository(UserLoginHistory)
		private readonly repository: Repository<UserLoginHistory>
	) {}

	public async addLoginHistory(email: string, ip: string, type: UserTryLoginStatus) {
		this.log.debug(`Try user ${email} user login from ip ${ip} type ${type}`);
		const entity = this.repository.create({ email, ip, type  });
		this.repository.save(entity);
	}
}