import { RootEntity } from '../../common/typeorm/root.entity';
import { Column, Entity } from 'typeorm';
import { UserTryLoginStatus } from './user-try-login-status';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
@Entity('user-login-history')
export class UserLoginHistory extends RootEntity {
  @Field()
  @Column({ nullable: false })
  email: string;

  @Field()
  @Column({ nullable: false })
  ip: string;

  @Field(() => UserTryLoginStatus)
  @Column({
    type: 'enum',
    enum: UserTryLoginStatus,
    enumName: 'UserTryLoginStatus',
    nullable: false,
    default: UserTryLoginStatus.None
  })
  type: UserTryLoginStatus;
}
