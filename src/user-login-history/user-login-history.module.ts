import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserLoginHistory } from './model/user-login-history.entity';
import { UserLoginHistoryService } from './user-login-history.service';

@Module({
	imports: [TypeOrmModule.forFeature([UserLoginHistory])],
	providers: [UserLoginHistoryService],
	exports: [UserLoginHistoryService],
})
export class UserLoginHistoryModule {}