export enum UserRole {
    SERVER = "SERVER",
    ADMIN = "ADMIN",
    USER_FREE = "USER_FREE",
    USER_PRO = "USER_PRO",
    NONE = "NONE",
}