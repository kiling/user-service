import bcrypt from 'bcrypt';
import { BeforeInsert, Column, Entity } from 'typeorm';
import { HASH_SALT_ROUNDS } from '../../config/config';
import { UserStatus } from './user-status';
import { Field, ObjectType } from '@nestjs/graphql';
import { RootEntity } from '../../common/typeorm/root.entity';
import { UserRole } from './user-role';
import { Paginated } from '../../common/pagination/pagination.response';

@ObjectType()
@Entity('user')
export class User extends RootEntity {
	@Field(() => UserRole)
	@Column({
		type: 'enum',
		enum: UserRole,
		enumName: 'UserRole',
		nullable: false,
		default: UserRole.NONE,
	})
	role: UserRole;

	@Field(() => UserStatus)
	@Column({
		type: 'enum',
		enum: UserStatus,
		enumName: 'UserStatus',
		nullable: false,
		default: UserStatus.NOT_ACTIVE,
	})
	status: UserStatus;

	@Field()
	@Column({ nullable: false, unique: true })
	email: string;

	@Field()
	@Column({ name: 'first_name', nullable: false })
	firstName: string;

	@Field()
	@Column({ name: 'sur_name', nullable: false })
	surName: string;

	@Column({ name: 'password', nullable: false })
	password: string;

	public toString(): string {
		return `${this.firstName} ${this.surName} (${this.email})`.trim();
	}

	@BeforeInsert()
	async transformData() {
		this.password = await bcrypt.hash(this.password, HASH_SALT_ROUNDS);
	}
}

@ObjectType()
export class PaginatedUser extends Paginated(User) {}