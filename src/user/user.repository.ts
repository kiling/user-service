import { Brackets, EntityRepository, SelectQueryBuilder } from 'typeorm';
import { PaginationRepository } from '../common/pagination/pagination.repository';
import { User } from './model/user.entity';

@EntityRepository(User)
export class UserRepository extends PaginationRepository<User> {
	constructor() {
		super(User);
	}

	protected createInitialQuery(): SelectQueryBuilder<User> {
		return this.createQueryBuilder();
	}

	public async findById(id_bigint: bigint): Promise<User> {
		let id = id_bigint.toString();
		return this.createQueryBuilder()
			.andWhere(new Brackets(qb => qb.where('User.id = :id', { id })))
			.getOne();
	}

	public async findByEmail(email: string): Promise<User> {
		return this.createQueryBuilder()
			.andWhere(new Brackets(qb => qb.where('User.email = :email', { email })))
			.getOne();
	}
}