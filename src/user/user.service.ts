import bcrypt from 'bcrypt';
import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PaginatedUser, User } from './model/user.entity';
import { UserRepository } from './user.repository';
import { UserErrorResponse } from './dto/response/user-error.response';
import { PaginationRequest } from '../common/pagination/pagination.request';
import { PAGINATION } from '../common/pagination/pagination.config';
import { UserStatus } from './model/user-status';
import { CreateUserRequest } from '../auth/request/create-user.request';
import { UserRole } from './model/user-role';
import { EmailAvailableResponse } from './dto/response/email-available.response';
import { UserLoginHistoryService } from '../user-login-history/user-login-history.service';
import { UserTryLoginStatus } from '../user-login-history/model/user-try-login-status';
import { ConfirmationCodeService } from '../confirmation-code/confirmation-code.service';
import { ConfirmationCodeType } from '../confirmation-code/model/confirmation-code-type';
import { CONFIRM_EMAIL } from '../config/config';

@Injectable()
export class UserService {
	private readonly log = new Logger(UserService.name);

	constructor(
		@InjectRepository(User)
		private readonly usersRepository: UserRepository,
		private readonly userLoginHistory: UserLoginHistoryService,
		private readonly confirmationCodeService: ConfirmationCodeService
	) {}

	public async getUserById(id: bigint): Promise<User> {
		const user = await this.usersRepository.findById(id);
		if (!user) throw new HttpException(UserErrorResponse.USER_NOT_FOUND_ERROR, HttpStatus.NOT_FOUND);
		return user;
	}

	async isEmailAvailable(email: string): Promise<EmailAvailableResponse> {
		this.log.debug(`Checking available username ${email}`);

		const user = await this.usersRepository.findByEmail(email);
		if (user) {
			return {
				isAvailable: false,
				email,
			};
		} else {
			return {
				isAvailable: true,
				email,
			};
		}
	}

	public async getUserByEmail(email: string): Promise<User> {
		const user = await this.usersRepository.findByEmail(email);
		if (!user) throw new HttpException(UserErrorResponse.USER_NOT_FOUND_ERROR, HttpStatus.NOT_FOUND);
		return user;
	}

	public async getPaginatedUsers(request: PaginationRequest): Promise<PaginatedUser> {
		const limit = Number(request.perPage) || PAGINATION.limit;
		const page = Number(request.page) || PAGINATION.page;
		this.log.debug(`Getting users page ${page} limit ${limit}`);

		const usersAndCount = await this.usersRepository.getPageAndCount(request);
		return {
			page,
			limit,
			data: usersAndCount[0],
			amount: usersAndCount[1],
		};
	}

	public async tryToLogin(email: string, password: string, ipAddres: string): Promise<User> {
		const user = await this.usersRepository.findByEmail(email);

		if (!user) {
			this.userLoginHistory.addLoginHistory(email, ipAddres, UserTryLoginStatus.LOGIN_USER_NOT_FOUND);
			throw new HttpException(UserErrorResponse.USER_NOT_FOUND_ERROR, HttpStatus.NOT_FOUND);
		}

		if (user.status === UserStatus.NOT_ACTIVE) {
			this.userLoginHistory.addLoginHistory(email, ipAddres, UserTryLoginStatus.LOGIN_USER_NOT_ACTIVE);
			throw new HttpException(UserErrorResponse.USER_NOT_ACTIVE_ERROR, HttpStatus.BAD_REQUEST);
		}

		const areEqual = await this._comparePasswords(password, user.password);

		if (!areEqual) {
			this.userLoginHistory.addLoginHistory(email, ipAddres, UserTryLoginStatus.LOGIN_INVALID_CREDENTIALS);
			throw new HttpException(UserErrorResponse.INVALID_CREDENTIALS_ERROR, HttpStatus.UNAUTHORIZED);
		}

		this.userLoginHistory.addLoginHistory(email, ipAddres, UserTryLoginStatus.LOGIN_SUCCESSFULLY);
		return user;
	}

	public async create(createUser: CreateUserRequest, ipAddres: string): Promise<User> {
		let userByEmail = await this.usersRepository.findByEmail(createUser.email);
		if (userByEmail) 
		{
			this.userLoginHistory.addLoginHistory(createUser.email, ipAddres, UserTryLoginStatus.CREATE_USER_ALREADY_EXIST);
			throw new HttpException(UserErrorResponse.USER_ALREADY_EXISTS_ERROR, HttpStatus.CONFLICT);
		}

		let newUser = this.usersRepository.create({
			...createUser,
			role: UserRole.USER_FREE,
			status: CONFIRM_EMAIL ? UserStatus.NOT_ACTIVE : UserStatus.ACTIVE,
		});
		newUser = await this.usersRepository.save(newUser);
		this.log.log(`Create new user - ${newUser.toString()}`);

		this.userLoginHistory.addLoginHistory(createUser.email, ipAddres, UserTryLoginStatus.CREATE_SUCCESSFULLY);

		if (CONFIRM_EMAIL)
		{
			// TODO: NotificationModel
			let confirmationCode = await this.confirmationCodeService.create(newUser.id, ConfirmationCodeType.ConfirmEmail);
			this.log.log(`Send confirmation token ${confirmationCode.token}`);
		}

		return newUser;
	}


	public async confirmEmail(token: string, ipAddres: string): Promise<boolean> {
		this.log.debug(`Confirming email for token ${token}`);

		var confirmationCode = await this.confirmationCodeService.find(token, ConfirmationCodeType.ConfirmEmail);
		if (!confirmationCode) {
			this.log.error(`User for tocken ${token} not found`);
			this.userLoginHistory.addLoginHistory("", ipAddres, UserTryLoginStatus.CONFIRMATION_CODE_NOT_VALID);
			throw new HttpException(
				UserErrorResponse.CONFIRMATION_CODE_NOT_VALID_ERROR,
				HttpStatus.BAD_REQUEST
			);
		}

		const user = await this.usersRepository.findById(confirmationCode.userId);
		if (!user) {
			this.log.error(`User for token ${token} not found`);
			this.userLoginHistory.addLoginHistory("", ipAddres, UserTryLoginStatus.CONFIRMATION_CODE_NOT_VALID);
			throw new HttpException(
				UserErrorResponse.CONFIRMATION_CODE_NOT_VALID_ERROR,
				HttpStatus.BAD_REQUEST
			);
		}
		
		user.status = UserStatus.ACTIVE;
		await this.usersRepository.save(user);
		await this.confirmationCodeService.delete(
			confirmationCode.token,
			ConfirmationCodeType.ConfirmEmail
		);
		this.userLoginHistory.addLoginHistory("", ipAddres, UserTryLoginStatus.CONFIRMATION_SUCCESSFULLY);
		return true;
	}

	async _comparePasswords(userPassword: string, currentPassword: string): Promise<boolean> {
		return await bcrypt.compare(userPassword, currentPassword);
	}
}
