import { Field, ObjectType } from '@nestjs/graphql';
import { IsBoolean, IsString } from 'class-validator';

@ObjectType()
export class EmailAvailableResponse {
	@Field()
	@IsString()
	email: string;

	@Field()
	@IsBoolean()
	isAvailable: boolean;
}