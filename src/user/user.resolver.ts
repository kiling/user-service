import { UseGuards } from '@nestjs/common';
import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { GraphQLBoolean, GraphQLString } from 'graphql';
import { LoggedUser } from '../common/auth/model/logged-user';
import { GqlAuthGuard, RoleGuard, Roles } from '../common/auth';
import { PaginationRequest } from '../common/pagination/pagination.request';
import { UserRole } from './model/user-role';
import { PaginatedUser, User } from './model/user.entity';
import { UserService } from './user.service';
import { GetLoggedUser } from '../auth/login-user.decorator';
import { EmailAvailableResponse } from './dto/response/email-available.response';
import { IpAddress } from '../auth/logged-user.decorator';

@Resolver(() => User)
export class UserResolver {
  constructor(private readonly userService: UserService) {}

  @Query(() => LoggedUser, {
    description: 'Roles ADMIN | USER_FREE | USER_PRO',
  })
  @UseGuards(GqlAuthGuard, RoleGuard)
  @Roles([
    UserRole.ADMIN,
    UserRole.SERVER,
    UserRole.USER_FREE,
    UserRole.USER_PRO,
  ])
  public async getCurrentUser(@GetLoggedUser() user: LoggedUser) {
    return user;
  }

  @Query(() => EmailAvailableResponse)
  public async isEmailAvailable(
    @Args('email', { type: () => GraphQLString }) email: string,
  ) {
    return this.userService.isEmailAvailable(email);
  }

  @Query(() => User, {
    description: 'Roles ADMIN',
  })
  @UseGuards(GqlAuthGuard, RoleGuard)
  @Roles([UserRole.ADMIN, UserRole.SERVER])
  public async getUserByEmail(
    @Args('email', { type: () => GraphQLString }) email: string,
  ) {
    return this.userService.getUserByEmail(email);
  }

  @Query(() => PaginatedUser, {
    description: 'Roles ADMIN',
  })
  @UseGuards(GqlAuthGuard, RoleGuard)
  @Roles([UserRole.ADMIN, UserRole.SERVER])
  public async getUsers(
    @Args('request', { type: () => PaginationRequest })
    request: PaginationRequest,
  ) {
    return this.userService.getPaginatedUsers(request);
  }

  @Mutation(() => GraphQLBoolean)
  public async confirmEmail(
    @Args('token', { type: () => GraphQLString }) token: string,
    @IpAddress() ipAddress: string,
  ) {
    return this.userService.confirmEmail(token, ipAddress);
  }
}
