import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from '@nestjs/common';
import { GqlArgumentsHost, GqlExceptionFilter } from '@nestjs/graphql';
import { ValidationException } from './validation.exception';

@Catch(ValidationException)
export class ValidationFilter implements GqlExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost) {
    return exception as ValidationException;
  }
}