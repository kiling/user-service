import {
	ArgumentsHost,
	Catch,
	ExceptionFilter,
	HttpException,
	HttpStatus,
	Logger,
} from '@nestjs/common';
import { GqlExceptionFilter, GqlExecutionContext } from '@nestjs/graphql';

@Catch(HttpException)
export class HttpExceptionsFilter implements GqlExceptionFilter {
	catch(exception: HttpException, host: ArgumentsHost): void {

		// const ctx = GqlExecutionContext.create(host);
    	// const user = ctx.getContext().req.user as LoggedUser;

		/*const statusCode = exception.getStatus();
		let message = exception.getResponse();

		// @ts-ignore
		if (Array.isArray(message.message)) {
			// @ts-ignore
			message = message.message[0];
		}

		if (typeof message !== 'string') {
			// @ts-ignore
			message = message.message?.toUpperCase();
		}*/

		Logger.error(exception);
		
		//const response = host.switchToHttp().getResponse();
		//response.status(statusCode).json({ statusCode, message });
	}
}

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
	catch(exception: Error, host: ArgumentsHost): void {
		/*const response = host.switchToHttp().getResponse();

		const statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
		const message = `${exception.message}`;*/
		Logger.error(exception.stack);

		//response.status(statusCode).json({ statusCode, message });
	}
}