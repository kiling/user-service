import { UserRole } from "../../../user/model/user-role";


export class JwtPayload {
	id: bigint;
	email: string;
	role: UserRole;
}