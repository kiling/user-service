import { Field, ObjectType } from "@nestjs/graphql";
import { UserRole } from "../../../user/model/user-role";

@ObjectType()
export class LoggedUser {
	id: bigint;
	@Field()
	email: string;
	@Field(() => UserRole)
	role: UserRole;
}
