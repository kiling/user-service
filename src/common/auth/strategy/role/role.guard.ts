import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { GqlExecutionContext } from '@nestjs/graphql';
import { UserErrorResponse } from '../../../../user/dto/response/user-error.response';
import { LoggedUser } from '../../model/logged-user';

@Injectable()
export class RoleGuard implements CanActivate {

	constructor(
		private readonly reflector: Reflector,
	) {
	}

	canActivate(context: ExecutionContext): boolean {
		const roles = this.reflector.get<string[]>('roles', context.getHandler());
		if (!roles) {
			return true;
		}
		const ctx = GqlExecutionContext.create(context);
    	const user = ctx.getContext().req.user as LoggedUser;
		if (!roles.includes(user.role)) {
			throw new HttpException(UserErrorResponse.NO_PERMISSIONS_ERROR, HttpStatus.FORBIDDEN)
		}
		return true;
	}
}