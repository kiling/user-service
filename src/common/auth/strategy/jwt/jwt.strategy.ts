import { Injectable, Logger } from '@nestjs/common';
import { JwtPayload } from '../../model/jwt-payload';
import { JWT_SETTINGS } from '../../config/auth-config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { LoggedUser } from '../../model/logged-user';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
	private readonly log = new Logger(JwtStrategy.name);

	constructor() {
		super({
			jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
			ignoreExpiration: false,
			secretOrKey: JWT_SETTINGS.auth.secret,
		});
	}

	public async validate(payload: JwtPayload): Promise<LoggedUser> {
		this.log.debug(`${JSON.stringify(payload)}`);
		return {
			id: payload.id,
			email: payload.email,
			role: payload.role,
		};
	}
}
