export const JWT_SETTINGS = {
    auth: {
        secret: process.env.JWT_SECRET_KEY || 'supersecret',
        expiresIn: process.env.JWT_EXPIRES_IN || '120m',
    },
    refresh: {
        secret: process.env.JWT_REFRESH_SECRET_KEY || 'superRefreshSecret',
        expiresIn: process.env.JWT_REFRESH_EXPIRES_IN || '30d',
    },
    refreshTokenName: 'Refresh',
};