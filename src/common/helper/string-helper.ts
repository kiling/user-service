export const capitalize = (value: string): string => {
  let newValue = value.trim();
  return newValue.charAt(0).toUpperCase() + newValue.slice(1);
};
