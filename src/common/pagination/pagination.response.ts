import { Field, ObjectType, Int } from '@nestjs/graphql';
import { Type } from '@nestjs/common';

export function Paginated<T>(classRef: Type<T>): any {
  @ObjectType({ isAbstract: true })
  abstract class PaginatedType {
    @Field((type) => [classRef], { nullable: true })
    data: T[];

    @Field((type) => Int)
    page: number;

    @Field((type) => Int)
    limit: number;

    @Field((type) => Int)
    amount: number;
  }
  return PaginatedType;
}