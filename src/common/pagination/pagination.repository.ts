import { ObjectLiteral, Repository, SelectQueryBuilder } from 'typeorm';
import { PAGINATION } from './pagination.config';
import { PaginationRequest } from './pagination.request';

export abstract class PaginationRepository<T> extends Repository<T> {
	private clazz: new () => T;

	constructor(x: new () => T) {
		super();
		this.clazz = x;
	}

	public async getPage(
		request: PaginationRequest,
		query?: [string, ObjectLiteral][],
		join?: [string, string][]
	): Promise<T[]> {
		return await this.createQuery(request, query, join).getMany();
	}

	public async getPageAndCount(
		request: PaginationRequest,
		query?: [string, ObjectLiteral][],
		join?: [string, string][]
	): Promise<[T[], number]> {
		return await this.createQuery(request, query, join).getManyAndCount();
	}

	protected abstract createInitialQuery(): SelectQueryBuilder<T>;

	private createQuery(
		request: PaginationRequest,
		query?: [string, ObjectLiteral][],
		join?: [string, string][]
	): SelectQueryBuilder<T> {
		const limit = Number(request.perPage) || PAGINATION.limit;
		const orderOptions = request.order || PAGINATION.orderBy;
		const page = Number(request.page) || PAGINATION.page;
		const skip = (page - 1) * limit;
		const orderBy = request.sortBy || PAGINATION.sortBy;

		const initialQuery = this.createInitialQuery();

		if (query && query.length > 0)
			query.forEach(q => initialQuery.andWhere(q[0], q[1]));
		if (join && join.length > 0) 
			join.forEach(j => initialQuery.leftJoinAndSelect(j[0], j[1]));

		return initialQuery
			.orderBy(`${this.clazz.name}.${orderBy}`, orderOptions)
			.skip(skip)
			.take(limit)
			.printSql();
	}
}
