import { IsEnum, IsNumberString, IsOptional, IsString } from 'class-validator';
import { OrderByOptions } from './pagination.config';
import { Int, Field, InputType } from '@nestjs/graphql';

@InputType()
export class PaginationRequest {
  @Field((type) => Int, { nullable: true })
  @IsOptional()
  page?: number;

  @Field((type) => Int, { nullable: true })
  @IsOptional()
  perPage?: number;

  @Field((type) => OrderByOptions, { nullable: true })
  @IsOptional()
  @IsEnum(OrderByOptions)
  order?: OrderByOptions;

  @Field({ nullable: true })
  @IsOptional()
  @IsString()
  sortBy?: string;
}
