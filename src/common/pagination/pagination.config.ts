export enum OrderByOptions {
    ASC = 'ASC',
    DESC = 'DESC',
}

export const PAGINATION = {
    limit: 5,
    page: 1,
    orderBy: OrderByOptions.ASC,
    sortBy: 'id',
};