import { Field, GraphQLISODateTime, ObjectType } from '@nestjs/graphql';
import GraphQLBigInt from 'graphql-bigint';
import {
  BaseEntity,
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export abstract class RootEntity extends BaseEntity {
  @Field(() => GraphQLBigInt, {
    description: 'entity id',
    nullable: false,
  })
  @PrimaryGeneratedColumn()
  id: bigint;

  @Field(() => GraphQLISODateTime, {
    description: 'creation date',
    nullable: false,
  })
  @CreateDateColumn()
  @Column({ nullable: false, name: 'createdat' })
  createdAt: Date;

  @Field(() => GraphQLISODateTime, {
    description: 'updated date',
    nullable: false,
  })
  @UpdateDateColumn()
  @Column({ nullable: false, name: 'updatedat' })
  updatedAt: Date;

  @BeforeInsert()
  @BeforeUpdate()
  updateDates(): void {
    const timestamp = new Date();
    this.updatedAt = timestamp;
    if (!this.createdAt) this.createdAt = timestamp;
  }
}
