import { Field } from '@nestjs/graphql';
import { GraphQLBoolean } from 'graphql';
import { Column } from 'typeorm';

import { RootEntity } from './root.entity';

export abstract class DeletableEntity extends RootEntity {
	@Field(() => GraphQLBoolean, {
		description: 'whether entity active or not',
		nullable: false,
		defaultValue: true,
	  })
	@Column({ name: 'active', default: true })
	active: boolean;
}