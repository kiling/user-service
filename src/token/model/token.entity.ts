import { RootEntity } from '../../common/typeorm/root.entity';
import { Column, Entity } from 'typeorm';
import { ObjectType } from '@nestjs/graphql';

@Entity('refresh-token')
export class Token extends RootEntity {
  @Column({ name: 'user_id', type: 'bigint', nullable: false })
  userId: bigint;

  @Column({ nullable: false })
  ip: string;

  @Column({ nullable: false })
  isRevoked: boolean;
}