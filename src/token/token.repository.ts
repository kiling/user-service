import { User } from '../entity/entities-index';
import { EntityRepository, Repository } from 'typeorm';
import { Token } from './model/token.entity'

@EntityRepository(Token)
export class TokenRepository extends Repository<Token> {
    
    public async createRefreshToken(user: User, ip: string): Promise<Token> {
        const token = new Token();
        token.userId = user.id;
        token.isRevoked = false;
        token.ip = ip;
        return token.save();
    }

    public async findTokenById(id_bigint: bigint): Promise<Token> {
        let id = id_bigint.toString();
        return this.findOne({ where: { id } });
    }
}