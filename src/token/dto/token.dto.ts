import { User } from '../../entity/entities-index';

export class TokenDto {
  token: string;
  expiresIn: string;
  user: User;
}
