import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { SignOptions, TokenExpiredError } from 'jsonwebtoken';
import { TokenRepository } from './token.repository';
import { Token } from './model/token.entity';
import { UserService } from '../user/user.service';
import { User } from '../entity/entities-index';
import { JWT_SETTINGS } from '../common/auth';
import { TokenDto } from './dto/token.dto';
import { UserErrorResponse } from '../user/dto/response/user-error.response';
import { UserLoginHistoryService } from '../user-login-history/user-login-history.service';
import { UserTryLoginStatus } from '../user-login-history/model/user-try-login-status';

export interface RefreshTokenPayload {
	iat: number; // Время создания рефреш токена
	exp: string; // Время истечения токена
	sub: string; // Id юзера
	jti: string; // Id рефреш токена
}

@Injectable()
export class TokenService {
	constructor(
		private readonly userService: UserService,
		private readonly userLoginHistory: UserLoginHistoryService,
		@InjectRepository(Token)
		private readonly tokens: TokenRepository,
		private readonly jwt: JwtService
	) {}

	public async generateAccessToken(user: User): Promise<TokenDto> {
		const expiresIn = JWT_SETTINGS.auth.expiresIn;
		const opts: SignOptions = {
			expiresIn,
			subject: String(user.id),
		};
		let token = await this.jwt.signAsync({ id: user.id, email: user.email, role: user.role }, opts);
		return { token, expiresIn, user };
	}

	public async generateRefreshToken(user: User, ip: string): Promise<TokenDto> {
		const expiresIn = JWT_SETTINGS.refresh.expiresIn;
		const tokenDb = await this.tokens.createRefreshToken(user, ip);
		const opts: SignOptions = {
			expiresIn,
			subject: String(user.id),
			jwtid: String(tokenDb.id),
		};
		let token = await this.jwt.signAsync({}, opts);
		return { token, expiresIn, user };
	}

	public async createAccessTokenFromRefreshToken(refresh: string, ipAddres: string): Promise<TokenDto> {
		const { user } = await this._resolveRefreshToken(refresh, ipAddres);
		return await this.generateAccessToken(user);
	}

	private async _resolveRefreshToken(encoded: string, ipAddres: string): Promise<{ user: User; token: Token }> {
		
		// PAYLOAD
		var payload: RefreshTokenPayload = null;
		try {
			payload = await this.jwt.verifyAsync(encoded);
		} catch (e) {
			if (e instanceof TokenExpiredError) 
			{
				this.userLoginHistory.addLoginHistory("", ipAddres, UserTryLoginStatus.REFRESH_TOKEN_EXPIRED);
				throw new UnprocessableEntityException(UserErrorResponse.REFRESH_TOKEN_EXPIRED);
			}
			else {
				this.userLoginHistory.addLoginHistory("", ipAddres, UserTryLoginStatus.REFRESH_TOKEN_MALFORMED);
				throw new UnprocessableEntityException(UserErrorResponse.REFRESH_TOKEN_MALFORMED);
			}
		}
		// USER
		const userId = payload.sub;
		if (!userId) 
		{
			this.userLoginHistory.addLoginHistory("", ipAddres, UserTryLoginStatus.REFRESH_TOKEN_MALFORMED);
			throw new UnprocessableEntityException(UserErrorResponse.REFRESH_TOKEN_MALFORMED);
		}
		const user = await this.userService.getUserById(BigInt(userId));
		if (!user) 
		{
			this.userLoginHistory.addLoginHistory("", ipAddres, UserTryLoginStatus.REFRESH_TOKEN_MALFORMED);
			throw new UnprocessableEntityException(UserErrorResponse.REFRESH_TOKEN_MALFORMED);
		}
		// TOKEN 
		const token = await this._getStoredTokenFromRefreshTokenPayload(payload);
		if (!token) 
		{
			this.userLoginHistory.addLoginHistory(user.email, ipAddres, UserTryLoginStatus.REFRESH_TOKEN_NOT_FOUND);
			throw new UnprocessableEntityException(UserErrorResponse.REFRESH_TOKEN_NOT_FOUND);
		}
	
		if (token.isRevoked) 
		{
			this.userLoginHistory.addLoginHistory(user.email, ipAddres, UserTryLoginStatus.REFRESH_TOKEN_REVOKED);
			throw new UnprocessableEntityException(UserErrorResponse.REFRESH_TOKEN_REVOKED);
		}
		this.userLoginHistory.addLoginHistory(user.email, ipAddres, UserTryLoginStatus.REFRESH_SUCCESSFULLY);
		return { user, token };
	}

	private async _getUserFromRefreshTokenPayload(payload: RefreshTokenPayload): Promise<User> {
		const userId = payload.sub;
		if (!userId) throw new UnprocessableEntityException(UserErrorResponse.REFRESH_TOKEN_MALFORMED);
		return await this.userService.getUserById(BigInt(userId));
	}

	private async _getStoredTokenFromRefreshTokenPayload(payload: RefreshTokenPayload): Promise<Token> {
		const tokenId = payload.jti;
		if (!tokenId) throw new UnprocessableEntityException(UserErrorResponse.REFRESH_TOKEN_MALFORMED);
		return this.tokens.findTokenById(BigInt(tokenId));
	}
}