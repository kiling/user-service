import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from '../user/user.module';
import { TokenService } from './token.service';
import { TokenRepository } from './token.repository';
import { JwtStrategy, JWT_SETTINGS } from '../common/auth';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UserLoginHistoryModule } from '../user-login-history/user-login-history.module';

@Module({
	imports: [
		UserModule,
		UserLoginHistoryModule,
		TypeOrmModule.forFeature([TokenRepository]),
		PassportModule.register({
			defaultStrategy: 'jwt',
			property: 'user',
			session: false,
		}),
		JwtModule.register({
			secret: JWT_SETTINGS.auth.secret,
		})],
	providers: [TokenService, JwtStrategy],
	exports: [TokenService, PassportModule, JwtModule],
})
export class TokenModule { }
