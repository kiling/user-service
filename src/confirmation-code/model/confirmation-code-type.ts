export enum ConfirmationCodeType {
	None = 'NONE',
	ChangePassword = 'CHANGE_PASSWORD',
	ConfirmEmail = 'CONFIRM_EMAIL',
}