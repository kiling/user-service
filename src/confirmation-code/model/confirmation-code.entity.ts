import { Column, Entity } from 'typeorm';
import { DeletableEntity } from '../../common/typeorm/deletable.entity';
import { ConfirmationCodeModule } from '../confirmation-code.module';
import { ConfirmationCodeType } from './confirmation-code-type';

@Entity('confirmation_code')
export class ConfirmationCode extends DeletableEntity {
	@Column({
		type: 'enum',
		enum: ConfirmationCodeType,
		enumName: 'ConfirmationCodeType',
		nullable: false,
		default: ConfirmationCodeType.None
	})
	type: ConfirmationCodeType;

	@Column({ name: 'user_id', type: 'bigint' })
	userId: bigint;

	@Column()
	token: string;
}