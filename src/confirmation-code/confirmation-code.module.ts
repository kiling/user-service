import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfirmationCode } from './model/confirmation-code.entity';
import { ConfirmationCodeService } from './confirmation-code.service';

@Module({
	imports: [TypeOrmModule.forFeature([ConfirmationCode])],
	providers: [ConfirmationCodeService],
	exports: [ConfirmationCodeService],
})
export class ConfirmationCodeModule {}