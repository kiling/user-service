import { EntityRepository, Repository } from 'typeorm';
import { ConfirmationCode } from './model/confirmation-code.entity';

@EntityRepository(ConfirmationCode)
export class ConfirmationCodeRepository extends Repository<ConfirmationCode> {}