import { Injectable, Logger } from '@nestjs/common';
import { ConfirmationCodeRepository } from './confirmation-code.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { ConfirmationCode } from './model/confirmation-code.entity';
import { ConfirmationCodeType } from './model/confirmation-code-type';
import { v4 as uuidv4 } from 'uuid';

@Injectable()
export class ConfirmationCodeService {
	private readonly log = new Logger(ConfirmationCodeService.name);

	constructor(
		@InjectRepository(ConfirmationCode)
		private readonly repository: ConfirmationCodeRepository
	) {}

	public async create(userId: bigint, type: ConfirmationCodeType) {
		this.log.debug(`Creating confirmation code for user ${userId} and type ${type}`);
		const entity = this.repository.create({
			userId,
			type,
			token: uuidv4()
		});

		return this.repository.save(entity);
	}

	public async findByUser(userId: bigint, type: ConfirmationCodeType) {
		this.log.debug(`Getting code for user ${userId}, type ${type}`);
		return this.repository.findOne({
			userId,
			type,
			active: true,
		});
	}

	public async find(token: string, type: ConfirmationCodeType) {
		return await this.repository.findOne({
			where: {
				token: token,
				active: true,
				type,
			},
		});
	}

	public async delete(code: string, type: ConfirmationCodeType) {
		this.log.debug(`Deleting code for code ${code}, type ${type}`);
		const entity = await this.find(code, type);

		if (entity) {
			entity.active = false;
			this.repository.save(entity);
		}
	}
}
